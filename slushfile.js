/*
* slush-npm-git-hooks
* https://github.com/danmartinez101/slush-npm-git-hooks
*
* Copyright (c) 2015, Daniel Martinez
* Licensed under the MIT license.
*/

'use strict';
if (!String.prototype.includes) {
  String.prototype.includes = function() {'use strict';
    return String.prototype.indexOf.apply(this, arguments) !== -1;
  };
}
var expect = require('gulp-expect-file');
var gulp = require('gulp'),
install = require('gulp-install'),
conflict = require('gulp-conflict'),
template = require('gulp-template'),
rename = require('gulp-rename'),
_ = require('underscore.string'),
inquirer = require('inquirer'),
path = require('path'),
jeditor = require("gulp-json-editor");

gulp.task('default', function (done) {
  var prompts = [{
    type: 'confirm',
    name: 'moveon',
    message: 'Continue?'
  }];
  //Ask
  inquirer.prompt(prompts,
    function (answers) {
      if (!answers.moveon) {
        return done();
      }
      var stepCount = 2;
      gulp.src(__dirname + '/templates/**')
      .pipe(conflict('./'))
      .pipe(gulp.dest('./'))
      .pipe(install())
      .on('end', function () {
        if (--stepCount == 0) {
          done();
        }
      });
      gulp.src("./package.json")
      .pipe(expect('package.json'))
      .pipe(jeditor(function(json) {
        json.scripts = json.scripts || {};
        json.scripts.prepublish = json.scripts.prepublish || "";
        if (!json.scripts.prepublish.includes('./bin/git/init-hooks || exit 0')) {
          if (json.scripts.prepublish.length !== 0) {
            json.scripts.prepublish += " && ";
          }
          json.scripts.prepublish += "./bin/git/init-hooks || exit 0";
        }
        json.devDependencies = json.devDependencies || {};
        json.devDependencies.jsup = json.devDependencies.jsup || "*";
        json.devDependencies.semver = json.devDependencies.semver || "*";

        return json;
      }))
      .pipe(gulp.dest("./"))
      .pipe(install())
      .on('end', function () {
        if(--stepCount == 0) {
          done();
        }
      });
    });
  });
